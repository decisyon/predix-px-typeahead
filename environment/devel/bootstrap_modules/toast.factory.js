(function() {
  'use strict';
  
  /**
   * Monica Modena
   * THe service DcyToastFactory uses the mdToast directive to view the message.
   */
  function DcyToastFactory($mdToast) {
	  
	  function serverError(errorMessage, options) {
		  
		  this.message = errorMessage;

		  var defaultOptions = {
				  position  : 'top right',
				  delay		: 0,
				  action	: 'OK',
				  highlightAction : true
		  };
		  
		  angular.merge(defaultOptions, options || {});
		  
		  $mdToast.show(
			        $mdToast.simple()
			          .content(errorMessage)
			          .position(defaultOptions.position)
			          .hideDelay(defaultOptions.delay) 
			          .action(defaultOptions.action)
			          .highlightAction(defaultOptions.highlightAction)
			    );
	  }
	  
	  var service = {
			  message: '',
			  serverError: serverError
	  };
		
	  return service;
	 
  }
  
  DcyToastFactory.$inject = ['$mdToast'];

  angular.module('dcyApp.factories').factory('dcyToastFactory', DcyToastFactory);

}());
