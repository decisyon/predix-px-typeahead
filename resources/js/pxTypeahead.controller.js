(function() {
	'use strict';
	/**
		* Controller to manage the Typeahead.
		* link:https://www.predix-ui.com/?show=px-typeahead&type=component
		*
	*/
	function PxTypeAheadCtrl($scope, $timeout, $element) {
		var ctrl = this,
		typeAhead = $('px-typeahead', $element),
		ctx = $scope.DECISYON.target.content.ctx,
		refObjId = ctx.instanceReference.value,
		target = $scope.DECISYON.target,
		mshPage = target.page,
		unBindWatches = [],
		paramName = '_PARAM_' + ctx.baseName.value,
		EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
		pxtypeAhead;
	
		/* Set the callback for data connector*/
		var setCallbackDC = function(dataConnector) {
			dataConnector.instance.data.then(function success(data) {
				setTypeaheadData(data);
			},function error(rejection) {
				setTypeaheadInError(rejection);
			});
		};

		/* Populate the data fetched through controller */
		var setTypeaheadData = function(data) {
			ctrl.typeAheadJsonData = wrapData(data);
			ctrl.currentTpl = ctrl.typeahead;
		};

		/* Function to handle DC failure */
		var setTypeaheadInError = function(rejection) {
			ctrl.errorMsg = rejection.errorMsg;
			ctrl.currentTpl = ctrl.errorView;
		};

		/* Get data from the associated data connector*/
		var getData = function() {
			var dc,
			useDataConnector = ctx.useDataConnector.value;
			if (useDataConnector) {
				if (target.getDataConnector) {
					dc = target.getDataConnector();
					dc.ready(function(dataConnector) {
						dataConnector.onUpdate(function(data) {
							setCallbackDC(dataConnector);
						});
						setCallbackDC(dataConnector);
					});
				} else {
					setTypeaheadInError({errorMsg : 'The widget can\'t be inizialized because it needs to be associated to a data connector.'});
					ctrl.typeAheadJsonData = [];
				}
			} else {
				setTypeaheadInError({errorMsg : 'The Use data connector property is set to false.'});
			}
		};

		/* Destroy the widget reference*/
		var destroyWidget = function() {
			var i = 0;
			for (i; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};
		
		/* Search the id into entries list by val. */
		var getEntryFromListByDesc = function(descToSearch) {
			return getEntryFromList(function(obj) {
				return obj == descToSearch;
			});
		};

		/* Search the id into entries list by callback. If not exist it returns false, otherwise the entire object found. */
		var getEntryFromList = function(filterFn) {
			if	(!angular.isArray(ctrl.typeAheadJsonData) || !angular.isFunction(filterFn)){
				return false;
			}
			var entry = ctrl.typeAheadJsonData.filter(filterFn);

			return !entry.length ? false : entry[0];
		};

		/* Handles the onBlur and itemSelected Events when we enter anything in typeahead*/
		function setEventHandler() {
			/* Exporting the param value */
			var sendParam = function(paramValue) {
				if (mshPage){
					target.sendParamChanged(ctrl.baseName, paramValue);
				}
			};

			/* Selects the highlighted text and passes to export parameter */
			var onItemSelected = function(event) {
				sendParam(event.target.inputValue);
			};

			/* Highlighted text is send to export param when user clicks tab/outside the textbox */
			var onBlurr = function(event) {
				/*this to check the value on blur in the input box id within the list then only populate in export param*/
				if (event.currentTarget.localName === 'input' && getEntryFromListByDesc(event.currentTarget.value)) {
					sendParam(event.currentTarget.value);
				}
			};

			/* Fetches the typeahead widget on the DOM and attaches the event listeners for blur and item-selected events */
			pxtypeAhead.$.search.addEventListener('blur', onBlurr);
			pxtypeAhead.addEventListener('px-typeahead-item-selected', onItemSelected);
		}
		
		/* Check imported parameters: if defined, if empty */
		function importParam(context) {
			var param = context[paramName],
				textMatchedWithExisting;
			ctrl.selectedValue = (angular.isDefined(param) && !angular.equals(param.value, EMPTY_PARAM_VALUE)) ? param.value : '';
			pxtypeAhead.$.search.value = ctrl.selectedValue;
		}


		/* Watch on widget context */
		function listenerOnDataChange() {
			unBindWatches.push($scope.$watch('DECISYON.target.content.ctx', function(newContext, oldContext) {
				if (!angular.equals(newContext, oldContext)) {
					ctrl.placeholder = (newContext.$pxTHPlaceholder.value !== '') ? newContext.$pxTHPlaceholder.value : '';
					importParam(newContext);
				}
			}, true));
			/* Listener on destroy angular element; in this case we destroy all watch*/
			$scope.$on('$destroy', destroyWidget);
		}

		/* Wrap a Array[Object] in a Array[String] */
		function wrapData(data) {
			var result = [];
			if (angular.isDefined(data) && angular.isArray(data)) {
				data.forEach(function(object) {
					result.push(object.val);
				});
			}
			return result;
		}

		/* Initialize block */
		function initialize() {
			ctrl.widgetID = 'pxTypeAhead_' + refObjId;
			ctrl.typeahead = 'pxtypeahead.html';
			ctrl.errorView = 'error.html';
			ctrl.placeholder = (ctx.$pxTHPlaceholder.value !== '') ? ctx.$pxTHPlaceholder.value : '';
			ctrl.baseName = angular.isDefined(ctx.baseName) ? ctx.baseName.value : '';
			ctrl.selectedValue = '';
			getData(ctx);
			listenerOnDataChange();
		}

		/* When the predix ui widget was completed. Callback called when dcyOnLoad directive has completed. */
		ctrl.setPredixTypeAheadLibraryLoaded = function()  {
			if (typeAhead) {
				pxtypeAhead = document.querySelector('#' + ctrl.widgetID);
				importParam(ctx);
				setEventHandler();
			}
		};

		/* Initialize block */
		initialize();
	}
	PxTypeAheadCtrl.$inject = ['$scope', '$timeout', '$element'];
	DECISYON.ng.register.controller('pxTypeAheadCtrl', PxTypeAheadCtrl);

}());